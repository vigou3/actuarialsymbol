### -*-Makefile-*- to setup package actuarialsymbol
##
## Copyright (C) 2018 Vincent Goulet
##
## 'make pkg' extracts the package file from the dtx file.
##
## 'make doc' compiles the documentation and glossary.
##
## 'make zip' creates an archive compliant with CTAN requirements.
##
## 'make release' creates a release on GitLab.
##
## 'make all' does 'pkg', 'doc' and 'zip'.
##
## Author: Vincent Goulet
##
## This file is part of project actuarialsymbol
## https://gitlab.com/vigou3/actuarialsymbol

## Package name on CTAN
PACKAGENAME = actuarialsymbol

## GitLab repository name
REPOSURL = https://gitlab.com/vigou3/actuarialsymbol

## Contents of the package (except README.md that is created by 'make
## zip')
FILES = ${PACKAGENAME}.dtx ${PACKAGENAME}.pdf mosaic.jpg

## Version number and release date extracted from dtx file. The result
## is a string of the form 'x.y (YYYY-MM-DD)'.
VERSION = $(shell awk -F '[ \[]' '/\ProvidesPackage/ \
	    { gsub(/\//, "-", $$2); \
	      printf("%s (%s)", substr($$3, 2), $$2); \
	      exit }' actuarialsymbol.dtx)

## Toolset
LATEX = pdflatex
MAKEINDEX = makeindex
CP = cp -p
RM = rm -r

## GitLab repository and authentication
REPOSNAME = $(shell basename ${REPOSURL})
APIURL = https://gitlab.com/api/v4/projects/vigou3%2F${REPOSNAME}
OAUTHTOKEN = $(shell cat ~/.gitlab/token)

## Automatic variable
TAGNAME = v$(word 1,${VERSION})


all : pkg doc zip

pkg : ${PACKAGENAME}.dtx
	${LATEX} $<

doc : ${PACKAGENAME}.dtx ${PACKAGENAME}.gls
	${LATEX} ${PACKAGENAME}.dtx
	${MAKEINDEX} -s gglo.ist -o ${PACKAGENAME}.gls ${PACKAGENAME}.glo
	${LATEX} ${PACKAGENAME}.dtx

zip : ${FILES}
	if [ -d ${PACKAGENAME} ]; then ${RM} ${PACKAGENAME}; fi
	mkdir ${PACKAGENAME}
	touch ${PACKAGENAME}/README.md && \
	  awk 'state==0 && /^# / { state=1 }; \
	       /^## Author/ { printf("## Version\n\n%s\n\n", "${VERSION}") } \
	       state' README.md >> ${PACKAGENAME}/README.md
	${CP} ${FILES} ${PACKAGENAME}
	zip --filesync -r ${PACKAGENAME}.zip ${PACKAGENAME}
	${RM} ${PACKAGENAME}

release :
	@echo ----- Creating release on GitLab...
	@if [ -n "$(shell git status --porcelain | grep -v '^??')" ]; then \
	    echo "uncommitted changes in repository; not creating release"; exit 2; fi
	@if [ -n "$(shell git log origin/master..HEAD)" ]; then \
	    echo "unpushed commits in repository; pushing to origin"; \
	      git push; fi
	if [ -e relnotes.in ]; then ${RM} relnotes.in; fi
	touch relnotes.in
	awk 'BEGIN { FS = "{"; \
	             v = substr("${TAGNAME}", 2); \
	             printf "{\"tag_name\": \"%s\", \"description\": \"# Version %s\\n", \
		            "${TAGNAME}", "${VERSION}" } \
	     /\\changes/ && substr($$2, 1, length($$2) - 1) == v { \
	         out = $$4; \
	         if ((i = index($$4, "}")) != 0) { \
	             out = substr($$4, 1, i - 1) \
	         } else { \
	             while (i == 0) { \
	                 getline; \
	                 gsub(/^%[ \t]+/, "", $$0); \
	                 i = index($$0, "}"); \
	                 if (i != 0) { \
	                     out = out " " substr($$0, 1, i - 1) \
			 } else { \
			     out = out " " $$0 \
		         } \
		     } \
	         } \
		 printf "- %s\\n", out \
	     } \
	     END { print "\"}" }' \
	     ${PACKAGENAME}.dtx >> relnotes.in
	curl --request POST \
	     --header "PRIVATE-TOKEN: ${OAUTHTOKEN}" \
	     "${APIURL}/repository/tags?tag_name=${TAGNAME}&ref=master"
	curl --data @relnotes.in \
	     --header "PRIVATE-TOKEN: ${OAUTHTOKEN}" \
	     --header "Content-Type: application/json" \
	     ${APIURL}/repository/tags/${TAGNAME}/release
	${RM} relnotes.in
	@echo ----- Done creating the release
